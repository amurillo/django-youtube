from django.apps import AppConfig
from .ytparser import YTChannel
import urllib.request


class YoutubeConfig(AppConfig):
    name = 'youtube'

    def ready(self):
        from .models import SelectedVideos, NotSelectedVideos
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
        xml_stream = urllib.request.urlopen(url)
        channel = YTChannel(xml_stream)
        videos = channel.videos()
        not_selected_videos = NotSelectedVideos.objects.all()
        not_selected_videos.delete()
        selected_videos = SelectedVideos.objects.all()
        selected_videos.delete()

        for video in videos:
            v = NotSelectedVideos(title=video['title'],
                                  url=video['url'],
                                  id_video=video['id_video'])
            v.save()
