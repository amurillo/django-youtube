from django.shortcuts import render, redirect
from .models import SelectedVideos, NotSelectedVideos
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
@csrf_exempt
def index(request):
    if request.method == "POST":
        action = request.POST['action']
        id_video = request.POST['id']

        if action == "Add":
            video = NotSelectedVideos.objects.get(id_video=id_video)
            selected = SelectedVideos(title=video.title,
                                      id_video=video.id_video,
                                      url=video.url)
            selected.save()
            video.delete()
        elif action == "Delete":
            video = SelectedVideos.objects.get(id_video=id_video)
            not_selected = NotSelectedVideos(title=video.title,
                                             id_video=video.id_video,
                                             url=video.url)
            not_selected.save()
            video.delete()

        return redirect('index')

    selected_videos = SelectedVideos.objects.all()
    not_selected_videos = NotSelectedVideos.objects.all()
    context = {
        'selected_list': selected_videos,
        'notSelected_list': not_selected_videos,
    }

    return render(request, 'youtube/index.html', context)
