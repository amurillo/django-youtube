from django.db import models


# Create your models here.
class SelectedVideos(models.Model):
    title = models.CharField(max_length=128)
    url = models.URLField()
    id_video = models.CharField(max_length=128)


class NotSelectedVideos(models.Model):
    title = models.CharField(max_length=128)
    url = models.URLField()
    id_video = models.CharField(max_length=128)
