from django.contrib import admin
from .models import SelectedVideos, NotSelectedVideos


# Register your models here.
admin.site.register(SelectedVideos)
admin.site.register(NotSelectedVideos)
